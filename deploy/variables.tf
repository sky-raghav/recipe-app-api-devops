variable "prefix" {
  default = "sky"
}

variable "project" {
  default = "akash-raghav-devops"
}

variable "contact" {
  default = "aakash.raghav@3pillarglobal.com"
}